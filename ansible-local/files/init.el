;;; init --- Summary

;;; Commentary:

;;; Code:

(load-theme 'solarized-dark t)

(global-flycheck-mode)

(fset 'breakpoint
      "breakpoint()")

(setq-default flycheck-disabled-checkers '(python-flake8 python-mypy python-lint))

(setq-default js-indent-level 2)

(setq ispell-dictionary "french")

(require 'mu4e)

(setq
   mu4e-maildir (expand-file-name "~/mail") ;;
   mu4e-sent-folder   "/Sent"       ;; folder for sent messages
   mu4e-drafts-folder "/Drafts"     ;; unfinished messages
   mu4e-trash-folder  "/Trash"      ;; trashed messages
   mu4e-refile-folder "/Archive" ;;
   mu4e-get-mail-command "mbsync eo"
   mu4e-update-interval 300
   mu4e-compose-complete-max 200
   mu4e-change-filenames-when-moving t
   message-send-mail-function 'smtpmail-send-it
   message-citation-line-format "Le %e %B %Y à %Hh%M, %N a écrit :
"
   message-citation-line-function 'message-insert-formatted-citation-line
   message-signature t
   user-full-name  "Emmanuel Cazenave"
   user-mail-address "ecazenave@entrouvert.com"
   smtpmail-smtp-server "smtp.entrouvert.org"
   smtpmail-smtp-service 587
   )

(add-to-list 'mu4e-bookmarks
  '( :name "Reply to travail"
     :query "maildir:/reply-to-travail AND flag:unread"
     :key  ?e)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Gestion redmine"
     :query "maildir:/gestion-redmine AND flag:unread"
     :key  ?r)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Tracim"
     :query "maildir:/tracim AND flag:unread"
     :key  ?m)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Club utilisateur"
     :query "maildir:/publik-club-utilisateur AND flag:unread"
     :key  ?u)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Famille"
     :query "maildir:/famille AND flag:unread"
     :key  ?f)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Gérant"
     :query "maildir:/gerant AND flag:unread"
     :key  ?o)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Prometheus"
     :query "maildir:/prometheus AND flag:unread"
     :key  ?p)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Sentry"
     :query "maildir:/sentry AND flag:unread"
     :key  ?s)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Clients"
     :query "maildir:/clients-reste AND flag:unread"
     :key  ?a)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Backup"
     :key  ?b
     :query "maildir:/clients-backup AND flag:unread")
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Juridique"
     :query "maildir:/juridique AND flag:unread"
     :key ?j)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Congés"
     :query "maildir:/conges AND flag:unread"
     :key ?h)
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Coopérateurs"
     :key  ?z
     :query "maildir:/cooperateurs AND flag:unread")
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Salariés"
     :key  ?s
     :query "maildir:/salaries AND flag:unread")
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Gestion"
     :key  ?g
     :query "maildir:/gestion AND flag:unread")
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Travail"
     :key  ?t
     :query "maildir:/travail AND flag:unread")
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Cpt"
     :key  ?c
     :query "maildir:/clients-cpt AND flag:unread")
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Inbox"
     :key  ?i
     :query "maildir:/Inbox AND flag:unread")
  )
(add-to-list 'mu4e-bookmarks
  '( :name "Assigné à moi"
     :key  ?a
     :query "maildir '/assigne-a-moi' AND flag:unread")
  )

(provide 'init)
;;; init.el ends here
