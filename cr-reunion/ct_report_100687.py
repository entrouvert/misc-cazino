# sudo -u wcs wcs-manage runscript -d formulaires-demarches.cr-reunion.fr ct_report.py > ct_report_complet.txt

from collections import defaultdict
import io
import os.path
import shutil
import subprocess
import tempfile
import time

from django.utils.encoding import force_bytes, force_str
from pypdf import PdfReader
from quixote import get_publisher
from xml.etree import ElementTree as ET

from wcs.formdef import FormDef
from wcs.carddef import CardDef
from wcs.wf.export_to_model import transform_opendocument
from wcs.workflows import get_formdata_template_context, template_on_context
import wcs.sql_criterias as st


OO_TEXT_NS = 'urn:oasis:names:tc:opendocument:xmlns:text:1.0'
OO_OFFICE_NS = 'urn:oasis:names:tc:opendocument:xmlns:office:1.0'
OO_STYLE_NS = 'urn:oasis:names:tc:opendocument:xmlns:style:1.0'
OO_DRAW_NS = 'urn:oasis:names:tc:opendocument:xmlns:drawing:1.0'
OO_FO_NS = 'urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0'
XLINK_NS = 'http://www.w3.org/1999/xlink'
USER_FIELD_DECL = '{%s}user-field-decl' % OO_TEXT_NS
USER_FIELD_GET = '{%s}user-field-get' % OO_TEXT_NS
SECTION_NODE = '{%s}section' % OO_TEXT_NS
SECTION_NAME = '{%s}name' % OO_TEXT_NS
STRING_VALUE = '{%s}string-value' % OO_OFFICE_NS
DRAW_FRAME = '{%s}frame' % OO_DRAW_NS
DRAW_NAME = '{%s}name' % OO_DRAW_NS
DRAW_IMAGE = '{%s}image' % OO_DRAW_NS
XLINK_HREF = '{%s}href' % XLINK_NS
NAME = '{%s}name' % OO_TEXT_NS



CT_CLASSIQUE = 'ct_classique'
CT_RESSOURCEMENT = 'ct_ressourcement'

carddef = CardDef.get(10)
formdef = FormDef.get(89)
target_formdatas = {CT_CLASSIQUE: {}, CT_RESSOURCEMENT: {}}


class ModelFile:

    def __init__(self, label):
        self.label = label
    
    def get_file(self):
        if self.label == CT_CLASSIQUE:
            return 'modele_bons_ct_avec_avoirs.odt'
        elif label == CT_RESSOURCEMENT:
            return 'modele_bons_ressourcement_avec_avoirs.odt'


def transform_to_pdf(instream):
    temp_dir = tempfile.mkdtemp()
    try:
        with tempfile.NamedTemporaryFile(dir=temp_dir) as infile:
            while True:
                chunk = instream.read(100000)
                if not chunk:
                    break
                infile.write(chunk)
            infile.flush()
            for dummy in range(3):
                lo_output = subprocess.run(
                    [
                        'libreoffice',
                        '-env:UserInstallation=file://%s' % temp_dir,
                        '--headless',
                        '--convert-to',
                        'pdf',
                        infile.name,
                        '--outdir',
                        temp_dir,
                    ],
                    check=True,
                    capture_output=True,
                )
                if os.path.exists(infile.name + '.pdf'):
                    break
                # sometimes libreoffice fails and sometimes it's ok
                # afterwards.
                time.sleep(0.5)
            if not os.path.exists(infile.name + '.pdf'):
                raise Exception(
                    'libreoffice failed to produce pdf (stdout: %r, stderr: %r)'
                    % (lo_output.stdout, lo_output.stderr)
                )
        with open(infile.name + '.pdf', 'rb') as fd:
            pdf_stream = io.BytesIO(fd.read())
        return pdf_stream
    except subprocess.CalledProcessError:
        raise Exception('libreoffice is failing')
    finally:
        shutil.rmtree(temp_dir)

    
def insert_form_details(node, formdata):
    section_node = node

    for field_action in formdata.get_summary_display_actions(None, include_unset_required_fields=False):
        if field_action['action'] == 'open-page':
            page_title = ET.SubElement(section_node, '{%s}h' % OO_TEXT_NS)
            page_title.attrib['{%s}outline-level' % OO_TEXT_NS] = '1'
            page_title.attrib['{%s}style-name' % OO_TEXT_NS] = 'Page_20_Title'
            page_title.text = field_action['value']
        elif field_action['action'] == 'title':
            title = ET.SubElement(section_node, '{%s}h' % OO_TEXT_NS)
            title.attrib['{%s}outline-level' % OO_TEXT_NS] = '2'
            title.attrib['{%s}style-name' % OO_TEXT_NS] = 'Form_20_Title'
            title.text = field_action['value']
        elif field_action['action'] == 'subtitle':
            title = ET.SubElement(section_node, '{%s}h' % OO_TEXT_NS)
            title.attrib['{%s}outline-level' % OO_TEXT_NS] = '3'
            title.attrib['{%s}style-name' % OO_TEXT_NS] = 'Form_20_Subtitle'
            title.text = field_action['value']
        elif field_action['action'] == 'comment':
            # comment can be free form HTML, ignore them.
            pass
        elif field_action['action'] == 'label':
            if not field_action['field'].get_opendocument_node_value:
                # unsupported field type
                continue
            label_p = ET.SubElement(section_node, '{%s}p' % OO_TEXT_NS)
            label_p.attrib['{%s}style-name' % OO_TEXT_NS] = 'Field_20_Label'
            label_p.text = field_action['value']
        elif field_action['action'] == 'value':
            if not field_action['field_value_info']['field'].get_opendocument_node_value:
                # unsupported field type
                continue
            value = field_action['value']
            if value is None:
                unset_value_p = ET.SubElement(section_node, '{%s}p' % OO_TEXT_NS)
                unset_value_p.attrib['{%s}style-name' % OO_TEXT_NS] = 'Field_20_Value'
                unset_value_i = ET.SubElement(unset_value_p, '{%s}span' % OO_TEXT_NS)
                unset_value_i.text = _('Not set')
            else:
                node_value = field_action['field_value_info']['field'].get_opendocument_node_value(
                    field_action['field_value_info']['value'], formdata
                )
                if node_value is None:
                    continue
                if isinstance(node_value, list):
                    for node in node_value:
                        section_node.append(node)
                        node.attrib['{%s}style-name' % OO_TEXT_NS] = 'Field_20_Value'
                elif node_value.tag in ('{%s}span' % OO_TEXT_NS, '{%s}a' % OO_TEXT_NS):
                    value_p = ET.SubElement(section_node, '{%s}p' % OO_TEXT_NS)
                    value_p.attrib['{%s}style-name' % OO_TEXT_NS] = 'Field_20_Value'
                    value_p.append(node_value)
                else:
                    node_value.attrib['{%s}style-name' % OO_TEXT_NS] = 'Field_20_Value'
                    section_node.append(node_value)


def apply_od_template_to_formdata(formdata, context, model_file):
    context.update(get_formdata_template_context(formdata))

    def process_styles(root):
        styles_node = root.find('{%s}styles' % OO_OFFICE_NS)
        if styles_node is None:
            return
        style_names = {x.attrib.get('{%s}name' % OO_STYLE_NS) for x in styles_node}
        for style_name in [
            'Page_20_Title',
            'Form_20_Title',
            'Form_20_Subtitle',
            'Field_20_Label',
            'Field_20_Value',
        ]:
            # if any style name is defined, don't alter styles
            if style_name in style_names:
                return
        for i, style_name in enumerate(
            ['Field_20_Label', 'Field_20_Value', 'Form_20_Subtitle', 'Form_20_Title', 'Page_20_Title']
        ):
            style_node = ET.SubElement(styles_node, '{%s}style' % OO_STYLE_NS)
            style_node.attrib['{%s}name' % OO_STYLE_NS] = style_name
            style_node.attrib['{%s}display-name' % OO_STYLE_NS] = style_name.replace('_20_', ' ')
            style_node.attrib['{%s}family' % OO_STYLE_NS] = 'paragraph'
            para_props = ET.SubElement(style_node, '{%s}paragraph-properties' % OO_STYLE_NS)
            if 'Value' not in style_name:
                para_props.attrib['{%s}margin-top' % OO_FO_NS] = '0.5cm'
            else:
                para_props.attrib['{%s}margin-left' % OO_FO_NS] = '0.25cm'
            if 'Title' in style_name:
                text_props = ET.SubElement(style_node, '{%s}text-properties' % OO_STYLE_NS)
                text_props.attrib['{%s}font-size' % OO_FO_NS] = '%s%%' % (90 + i * 10)
                text_props.attrib['{%s}font-weight' % OO_FO_NS] = 'bold'

    def process_root(root, new_images):
        if root.tag == '{%s}document-styles' % OO_OFFICE_NS:
            return process_styles(root)

        # cache for keeping computed user-field-decl value around
        user_field_values = {}

        def process_text(t):
            return template_on_context(context, force_str(t), autoescape=False, record_errors=False, raises=True)

        nodes = []
        for node in root.iter():
            nodes.append(node)
        for node in nodes:
            got_blank_lines = False
            if node.tag == SECTION_NODE and 'form_details' in node.attrib.get(SECTION_NAME, ''):
                # custom behaviour for a section named form_details
                # (actually any name containing form_details), create
                # real odt markup.
                children = [x for x in node]
                for child in children:
                    node.remove(child)
                insert_form_details(node, formdata)

            # apply template to user-field-decl and update user-field-get
            if node.tag == USER_FIELD_DECL and STRING_VALUE in node.attrib:
                node.attrib[STRING_VALUE] = process_text(node.attrib[STRING_VALUE])
                if NAME in node.attrib:
                    user_field_values[node.attrib[NAME]] = node.attrib[STRING_VALUE]
            if (
                node.tag == USER_FIELD_GET
                and NAME in node.attrib
                and node.attrib[NAME] in user_field_values
            ):
                node.text = user_field_values[node.attrib[NAME]]

            for attr in ('text', 'tail'):
                if not getattr(node, attr):
                    continue
                old_value = getattr(node, attr)
                setattr(node, attr, process_text(old_value))
                new_value = getattr(node, attr)
                if old_value != new_value and '\n\n' in new_value:
                    got_blank_lines = True
            if got_blank_lines:
                # replace blank lines by forced line breaks (it would be
                # better to be smart about the document format and create
                # real paragraphs if we were inside a paragraph but then
                # we would also need to copy its style and what not).
                current_tail = node.tail or ''
                node.tail = None
                as_str = force_str(ET.tostring(node)).replace(
                    '\n\n', 2 * ('<nsa:line-break xmlns:nsa="%(ns)s"/>' % {'ns': OO_TEXT_NS})
                )
                as_node = ET.fromstring(as_str)
                node.text = as_node.text
                children = [x for x in node]
                for child in children:
                    node.remove(child)
                for child in as_node:
                    node.append(child)
                node.tail = current_tail

    outstream = io.BytesIO()
    transform_opendocument(model_file.get_file(), outstream, process_root)
    outstream.seek(0)
    return outstream



def get_target_cts(formdata):
    target_cts = {
        CT_CLASSIQUE: {},
        CT_RESSOURCEMENT: {},
    }

    for trace in formdata.get_workflow_traces():
        if trace.event_args and trace.event_args.get('external_formdef_id') == '10':
            ct = carddef.data_class().get(trace.event_args.get('external_formdata_id'))
            numero_bon = ct.data['2']
            assert numero_bon.startswith('B2024')
            assert numero_bon[5:8] in ('176', '177')
            if numero_bon[5:8] == '176':
              target_cts[CT_CLASSIQUE][numero_bon] = ct
            else:
                target_cts[CT_RESSOURCEMENT][numero_bon] = ct

    return target_cts


def read_invice_file(filepath):
    file_cts = defaultdict(int)
    reader = PdfReader(filepath)
    for page in reader.pages:
        for line in page.extract_text().splitlines():
            if line.startswith('• B'):
                ct = line.split(' ')[1]
                file_cts[ct] += 1
    return file_cts


# detect incorrect invoices
for formdata in formdef.data_class().select():
    if formdata.id not in (232, 229, 97):
        continue
    print('\n Loop %s' % formdata.get_backoffice_url())

    nb_bons_ct_annee_n = formdata.data.get('bo29bbd46d-ac2a-4b3b-b389-02f49a08dcb5')
    nb_bons_ressourcement_annee_n = formdata.data.get('bo11238d13-e124-43e3-856d-0984cfaf55db')
    if nb_bons_ct_annee_n == 0 and nb_bons_ressourcement_annee_n == 0:
        continue

    target_cts = get_target_cts(formdata)

    facture_recap_bons_ct_annee_n = formdata.data['bob0bfef59-0888-4f77-85da-995e6411b94b']
    facture_recap_ressourcement_annee_n = formdata.data['bo1a2303a0-56b2-40c4-9a8d-fbb3597bb5c0']

    formdata_printed = False
    for label, facture_file in [(CT_CLASSIQUE, facture_recap_bons_ct_annee_n), (CT_RESSOURCEMENT, facture_recap_ressourcement_annee_n)]:
        if not facture_file:
            continue

        file_cts = read_invice_file(facture_file.get_file_path())

        # detect doublon
        for ct, hit in file_cts.items():
            if hit > 1:
                target_formdatas[label][formdata.id] = formdata
                if not formdata_printed:
                    print('\n* %s' % formdata.get_backoffice_url())
                    formdata_printed = True
                print(' - doublon %s %s %s' % (label, ct, carddef.data_class().select([st.Equal('f2', ct)])[0].get_backoffice_url()))

        # detect missing
        for numero_bon, bon in target_cts[label].items():
            if numero_bon not in file_cts:
                target_formdatas[label][formdata.id] = formdata
                if not formdata_printed:
                    print('\n* %s' % formdata.get_backoffice_url())
                    formdata_printed = True
                print(' - absent %s %s %s' % (label, numero_bon, bon.get_backoffice_url()))


# generate invoice
for label, data in target_formdatas.items():
    for _, formdata in data.items():
        nb_bons_ct_annee_n = formdata.data.get('bo29bbd46d-ac2a-4b3b-b389-02f49a08dcb5')
        nb_bons_ressourcement_annee_n = formdata.data.get('bo11238d13-e124-43e3-856d-0984cfaf55db')
        target_cts = get_target_cts(formdata)
        tmp_target_cts = [x.get_as_lazy() for x in target_cts[label].values()]
        context = {
            'target_cts_0': tmp_target_cts[:100],
            'target_cts_1': tmp_target_cts[100:200],
            'target_cts_2': tmp_target_cts[200:300],
            'target_cts_3': tmp_target_cts[300:400],
            'target_cts_4': tmp_target_cts[400:500],
        }
        outstream = apply_od_template_to_formdata(formdata, context, ModelFile(label))
        outstream.seek(0)
        resstream = transform_to_pdf(outstream)
        resstream.seek(0)
        filepath = '100687/%s_%s.pdf' % (label, formdata.id)
        with open(filepath, 'wb') as f:
            f.write(resstream.read())

        file_cts = read_invice_file(filepath)

        formdata_printed = False
        # detect doublon
        for ct, hit in file_cts.items():
            if hit > 1:
                if not formdata_printed:
                    print('\n* %s' % formdata.get_backoffice_url())
                    formdata_printed = True
                print(' - doublon %s %s %s' % (label, ct, carddef.data_class().select([st.Equal('f2', ct)])[0].get_backoffice_url()))

        # detect missing
        for index, bon in enumerate(tmp_target_cts):
            numero_bon = str(bon.get('bon_numero'))
            if numero_bon not in file_cts:
                if not formdata_printed:
                    print('\n* %s' % formdata.get_backoffice_url())
                    formdata_printed = True
                print(' - absent %s %s %s %s' % (label, index, numero_bon, bon.get_backoffice_url()))
