import pandas as pd

demandes = pd.read_csv('/home/cazino/notes/clients/cr-reunion/aide-voyage/demande.csv')

demandes[['volet', 'type demande', 'type aide', 'mesure specifique']].drop_duplicates().to_csv('/home/cazino/notes/clients/cr-reunion/aide-voyage/mapping.csv')

demandes[demandes[['nom', 'prenom', 'numero fiscal']].duplicated(keep=False)].sort_values('numero fiscal').to_csv('/home/cazino/notes/clients/cr-reunion/aide-voyage/doublons.csv')

demandes[demandes[['nom', 'prenom', 'numero fiscal']].duplicated(keep=False)].sort_values(['numero fiscal', 'prenom']).to_csv('/home/cazino/notes/clients/cr-reunion/aide-voyage/tmp.csv')
