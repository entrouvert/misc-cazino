"""
https://dev.entrouvert.org/issues/56418

Usage :

sudo -u wcs wcs-manage runscript -d demarches.toodego.com run.py
"""

from wcs.carddef import CardDef
from wcs.formdef import FormDef

FORM_SLUG = 'demande-de-rs-jeunes'
CARD_SLUG = 'debug-integration-adresse-rsj'
FIELDS_INDEX = {
    'adresse': '105',
    'numero': '106',
    'voie': '107',
    'code_postal': '108',
    'commune': '109',
    'complement': '110',
    'insertis': '16'
}


def get_adresse_cache():
    res = {}
    cardef = CardDef.get_by_urlname(CARD_SLUG)
    for carddata in cardef.data_class().select():
        data = carddata.get_as_dict()
        id_insertis = data['var_id_insertis']
        adresse_verifiee_structured = data['var_adresse_verifiee_structured']
        res[id_insertis] = adresse_verifiee_structured
    return res


def main():
    count = 0
    adresse_cache = get_adresse_cache()
    formdef = FormDef.get_by_urlname(FORM_SLUG)
    fields_index = {}
    for field in formdef.fields:
        fields_index[field.id] = field

    for formdata in formdef.data_class().select():
        if formdata.status == 'draft':
            continue
        if formdata.data[FIELDS_INDEX['adresse']] is None:
            count += 1
            print(formdata.get_backoffice_url())
            # do the job
            id_insertis = formdata.data[FIELDS_INDEX['insertis']]
            if not adresse_cache.get(id_insertis):
                raise ValueError()

            adresse_data = adresse_cache.get(id_insertis)

            formdata.data[FIELDS_INDEX['adresse']] = adresse_data['id']
            formdata.data["%s_display" % FIELDS_INDEX['adresse']] = adresse_data['display_name']
            formdata.data["%s_structured" % FIELDS_INDEX['adresse']] = adresse_data

            if 'house_number' in adresse_data['address']:
                formdata.data[FIELDS_INDEX['numero']] = adresse_data['address']['house_number']
            formdata.data[FIELDS_INDEX['voie']] = adresse_data['address']['road']
            formdata.data[FIELDS_INDEX['code_postal']] = adresse_data['address']['postcode']
            formdata.data[FIELDS_INDEX['commune']] = adresse_data['address']['city']
            formdata.store()

    print(count)


main()
