# chrono-manage tenant_command -d xxxx shell
# exec(open('chrono_add_desks.py').read())

AGENDA_ID = xx # integer identifier of to agenda whom we want to add desks
NUM_DESK_TO_ADD = yy # integer num of des kto add

from datetime import datetime
from django.db import transaction
from chrono.agendas.models import Agenda, Desk, TimePeriod, TimePeriodException


agenda = Agenda.objects.get(pk=AGENDA_ID)
# grab an 'old' desk since new ones might have special time periods exceptions
desk = agenda.desk_set.order_by('pk').first()


with transaction.atomic():
    for i in range(NUM_DESK_TO_ADD):
        desk_label = 'script desk %s %s' % (datetime.now().strftime('%d-%m-%Y %H:%S'), i+1)
        new_desk = Desk.objects.create(agenda=agenda, label=desk_label)

        # add time periods
        for tp in desk.timeperiod_set.all():
            TimePeriod.objects.create(
                desk=new_desk,
                start_time=tp.start_time,
                end_time=tp.end_time,
                weekday=tp.weekday
            )

        # add time periods exceptions
        for tpe in desk.timeperiodexception_set.all():
            TimePeriodException.objects.create(
                desk=new_desk,
                source=tpe.source,
                label=tpe.label,
                start_datetime=tpe.start_datetime,
                end_datetime=tpe.end_datetime,
                recurrence_id=tpe.recurrence_id
            )
