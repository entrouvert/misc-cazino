"""
Usage :
authentic2-multitenant-manage tenant_command runscript venissieux-technocarte/a2_run.py reset -d authentic.dev.publik.love

authentic2-multitenant-manage tenant_command runscript venissieux-technocarte/a2_run.py import --filepath ~/notes/clients/venissieux/famille/technocarte.csv -d authentic.dev.publik.love
"""

import argparse
import collections
import csv

from django.contrib.auth import get_user_model


User = get_user_model()


def get_rows(args):
    with open(args.filepath) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quotechar='"')
        for i, row in enumerate(reader):
            if args.mode != 'full' and i > args.sample_numlines:
                break
            yield row


def build_email(first_name, last_name):
    return 'vpf-%s-%s@yopmail.com' % (first_name, last_name)


def import_data(args):
    familles_adulte_count = collections.defaultdict(int)
    for row in get_rows(args):
        if row['ADULTE'] != '1':
            continue

        code_famille = row['CODFAM']
        if familles_adulte_count[code_famille] >= 2:
            # gestion des doublons
            continue

        data = {
            'last_name': row['NOMENF'],
            'first_name': row['PREENF'],
            'email': build_email(row['PREENF'], row['NOMENF'])
        }

        User.objects.create(**data)


def reset(args):
    User.objects.filter(email__startswith='vpf').filter(email__endswith='yopmail.com').delete()


parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()

parser_import = subparsers.add_parser('import')
parser_import.set_defaults(func=import_data)
parser_import.add_argument('--filepath')
parser_import.add_argument('--mode', default='sample', choices=('sample', 'full'))
parser_import.add_argument('--sample-numlines', default=100, type=int)

parser_reset = subparsers.add_parser('reset')
parser_reset.set_defaults(func=reset)

args = parser.parse_args()
args.func(args)
