require ["fileinto", "imap4flags"];

if allof (address :contains "to" "admin", address :contains "to" "entrouvert.com")
{
    fileinto :flags "\\Seen" "traces";
}

elsif allof (address :contains "to" "admin", address :contains "to" "entrouvert.org")
{
    fileinto :flags "\\Seen" "traces";
}

elsif address :is "to" [
  "docbow@entrouvert.org",
  "root@entrouvert.org",
  "root@localhost",
  "welco@entrouvert.org",
  "root@web1.preprod.publik-hds.entrouvert.pf.internet.wf"
  ]
{
    fileinto :flags "\\Seen" "traces";
}

elsif address :is "to" "postmaster@entrouvert.org"
{
    fileinto :flags "\\Seen" "postmaster";
}

elsif address :is ["from", "to", "cc"] "ao@listes.entrouvert.com"
{
    fileinto :flags "\\Seen" "ao";
}

elsif address :is ["from", "to", "cc"] "conges@listes.entrouvert.com"
{
    fileinto "conges";
}

elsif address :is ["from", "to", "cc"] "cooperateurs@listes.entrouvert.com"
{
    fileinto "cooperateurs";
}

elsif address :is ["from", "to", "cc"] "gandi@entrouvert.com"
{
    fileinto "gandi";
}

elsif address :is ["from", "to", "cc"] "social@entrouvert.com"
{
    fileinto "social";
}

elsif anyof (header :is "List-Id" "<gestion@listes.entrouvert.com>", address :is ["to", "cc"] ["gestion@listes.entrouvert.com", "gestion@entrouvert.com"], header :is "reply-to" ["gestion@listes.entrouvert.com", "gestion@entrouvert.com"])
{
    fileinto "gestion";
}

elsif address :is ["to", "cc"] "gt-integrations-graphiques@listes.entrouvert.com"
{
    fileinto "gt-integrations-graphiques";
}

elsif header :is "X-Redmine-Project" "gestion"
{
    fileinto "gestion-redmine";
}

elsif address :is ["from", "to", "cc"] "gerant@entrouvert.com"
{
    fileinto "gerant";
}

elsif address :is ["from", "to", "cc"] "famille@listes.entrouvert.com"
{
    fileinto "famille";
}

elsif address :is ["from", "to", "cc"] "info@entrouvert.com"
{
    fileinto "info";
}

elsif address :is ["from", "to", "cc"] "juridique@listes.entrouvert.com"
{
    fileinto "juridique";
}

elsif address :is ["from", "to", "cc"] "ovh@entrouvert.com"
{
    fileinto "ovh";
}

elsif address :is "to" "prometheus@entrouvert.org"
{
    fileinto "prometheus";
}

elsif address :is ["to", "cc"] "publik-club-utilisateur@listes.entrouvert.com"
{
    fileinto "publik-club-utilisateur";
}

elsif address :is  "from" "ne-pas-repondre@tracim.info"
{
    fileinto "publik-club-utilisateur";
}


elsif address :is ["from", "to", "cc"] ["noc@listes.libre-entreprise.org", "reseau@listes.libre-entreprise.org", "techno@listes.libre-entreprise.org"]
{
    fileinto "reseau-libre-entreprise";
}

elsif address :is ["from", "to", "cc"] ["travail@listes.entrouvert.com", "equipe@entrouvert.com"]
{
    fileinto "travail";
}

elsif address :is ["from", "to", "cc"] "eo-pc@entrouvert.com"
{
    fileinto "eo-pc";
}

elsif header :is "reply-to" "travail@listes.entrouvert.com"
{
    fileinto "reply-to-travail";
}

elsif address :is ["from", "to", "cc"] "salaries@listes.entrouvert.com"
{
    fileinto "salaries";
}

elsif address :is "from" "admin+sentry.entrouvert.org@entrouvert.org"
{
    fileinto "sentry";
}

elsif address :is "from" "ne-pas-repondre@tracim.fr"
{
    fileinto :flags "\\Seen" "tracim";
}

elsif header :is "X-Redmine-Issue-Assignee-To-Me" "true"
{
    fileinto "assigne-a-moi";
}

elsif header :is "X-Redmine-Project" "espace-des-cpf"
{
    fileinto :flags "\\Seen" "espace-cpf";
}

elsif anyof (header :is "X-Redmine-Project" [
    "auch",
    "cabourg",
    "cap-atlantique",
    "cd31",
    "corbas",
    "croix",
    "cr-reunion",
    "dynacteur-rouen",
    "grand-bourg-agglomeration",
    "hautes-pyrenees",
    "imio-connect",
    "mauguio",
    "metz",
    "meudon",
    "meuse-cd55",
    "moselle",
    "mrn",
    "pfwb-docbow",
    "pre-saint-gervais",
    "pw",
    "rouen",
    "saintchamond",
    "saumur-agglomeration",
    "sitiv",
    "universite-de-nantes",
    "universite-de-toulon",
    "universite-lorraine",
    "venissieux",
    "ville-d-avray"
    ], address :is ["from", "to", "cc"] [
    "cabourg-ccncpa@listes.entrouvert.com",
    "cap-atlantique@listes.entrouvert.com",
    "crrun-ams-quebec@listes.entrouvert.com",
    "crrun-pass-num@listes.entrouvert.com",
    "cr974@listes.entrouvert.com",
    "grand-bourg@listes.entrouvert.com",
    "hautes-pyrenees@listes.entrouvert.com",
    "mauguio@listes.entrouvert.com",
    "metz@listes.entrouvert.com",
    "meuse@listes.entrouvert.com",
    "moselle@listes.entrouvert.com",
    "saumur-agglomeration@listes.entrouvert.com",
    "univ-toulon@listes.entrouvert.com"
    ])
{
    fileinto "clients-cpt";
}

elsif allof (address :is "from" "redmine@entrouvert.com", header :contains "subject" "(Fermé)")
{
    fileinto :flags "\\Seen" "Trash";
}

elsif allof (address :is "from" "redmine@entrouvert.com", header :contains "subject" "Résolu (à déployer)")
{
    fileinto :flags "\\Seen" "Trash";
}

elsif allof (address :is "from" "redmine@entrouvert.com", header :contains "subject" "(Solution déployée)")
{
    fileinto :flags "\\Seen" "Trash";
}

elsif anyof (header :is "X-Redmine-Project" [
    "academie-versailles",
    "bron",
    "caluire-et-cuire",
    "champagne-au-mont-d-or",
    "cinor",
    "clisson",
    "cnil",
    "corbas-toodego",
    "gpseo",
    "grand-lyon",
    "grenoble-metropole",
    "imio",
    "le-havre",
    "malakoff",
    "marseille",
    "mel",
    "mincult",
    "mineduc",
    "oullins",
    "pfwb-publik",
    "pierre-benite",
    "saint-didier",
    "saint-fons",
    "saint-priest",
    "sitiv-venissieux-publik-famille",
    "tournai",
    "vaulx-en-velin"
    ], address :is ["from", "to", "cc"] [
    "academie-versailles@listes.entrouvert.com",
    "ccclisson@listes.entrouvert.com",
    "gers@listes.entrouvert.com",
    "gpseo@listes.entrouvert.com",
    "grenoble-metropole@listes.entrouvert.com",
    "marseille@listes.entrouvert.com",
    "mineduc@listes.entrouvert.com",
    "thonon-agglomeration@listes.entrouvert.com"
    ])
{
    fileinto "clients-backup";
}

elsif anyof (header :is "X-Redmine-Project" [
    "aix-marseille-metropole-amp",
    "allo-toulouse",
    "alpes-maritimes",
    "amiens",
    "angouleme",
    "antibes",
    "arles",
    "armentieres",
    "atreal",
    "berreletang",
    "bethune-bruay",
    "bouches-du-rhone-cd13",
    "caen-la-mer",
    "cah",
    "calvados",
    "calvados-portail-agents",
    "cannes",
    "cca",
    "cd15",
    "cd28",
    "cd29-finistere",
    "cd38-connecteur-tseh",
    "cd39",
    "cd44",
    "cd46-lot",
    "cd91",
    "chambery",
    "chateauroux",
    "chemdata",
    "clermont-metropole",
    "communes-grand-nancy",
    "connexion-logitud",
    "correze-cd19",
    "deploiement-multi-communes-nm",
    "dordogne-cd24",
    "dreux",
    "ehess",
    "eybens",
    "estensemble",
    "fondettes",
    "fontenaysousbois",
    "givors",
    "grandlyon-sau",
    "grand-chatellerault",
    "grand-lyon-instance-intranet",
    "grand-lyon-gi",
    "grand-lyon-instances-intranet-extranet",
    "grand-lyon-instance-grand-lyon-territoire",
    "grand-nancy",
    "grenoble-passage-au-multi-instance",
    "hautes-alpes",
    "integration-graphique",
    "isere",
    "isere-agents",
    "lambersart",
    "landes",
    "lanester",
    "la-hague-communaute-de-communes",
    "la-manche",
    "la-possession",
    "la-rochelle",
    "la-somme",
    "les-herbiers",
    "les-sables-d-olonne",
    "le-plessis-trevise",
    "lille",
    "lozere",
    "malakoff",
    "martigues",
    "meyzieu",
    "migration",
    "ministere-des-solidarites-et-de-la-sante",
    "miribel",
    "mnhn",
    "montreuil",
    "nancy",
    "nanterre-rsu",
    "nantes-metropole",
    "nice",
    "nimes",
    "nimes-famille",
    "nord",
    "noyellesgodault",
    "orleans",
    "parsifal-mairie-de-toulouse",
    "quimper",
    "roanne",
    "rochefort",
    "saint-denis-gru",
    "saint-laurent-du-var",
    "saone-et-loire",
    "seine-eure",
    "sens",
    "sictiam",
    "strasbourg",
    "saint-denis",
    "saint-genis-laval",
    "saint-lo",
    "seine-et-marne-cd77",
    "sictiam-publik-themes",
    "sve-ministere-de-l-interieur",
    "thononagglomeration",
    "thouare",
    "toulouse",
    "toulouse-technique",
    "tours",
    "universite-d-avignon",
    "usmb",
    "vandoeuvre",
    "villejuif",
    "villeneuve-d-ascq",
    "villeurbanne",
    "ville-de-grenoble",
    "ville-de-montpellier",
    "3m"
    ], address :is ["from", "to", "cc"] [
    "ampmetropole@listes.entrouvert.com",
    "angouleme@listes.entrouvert.com",
    "antibes@listes.entrouvert.com",
    "armentieres@listes.entrouvert.com",
    "arpajon@listes.entrouvert.com",
    "auch@listes.entrouvert.com",
    "berreletang@listes.entrouvert.com",
    "bethune-bruay@listes.entrouvert.com",
    "blagnac@listes.entrouvert.com",
    "braine-l-alleud@listes.entrouvert.com",
    "caen-la-mer@listes.entrouvert.com",
    "cah@listes.entrouvert.com",
    "cd05@listes.entrouvert.com",
    "cd06@listes.entrouvert.com",
    "cd13@listes.entrouvert.com",
    "cd15@listes.entrouvert.com",
    "cd19@listes.entrouvert.com",
    "cd24@listes.entrouvert.com",
    "cd28@listes.entrouvert.com",
    "cd29@listes.entrouvert.com",
    "cd50@listes.entrouvert.com",
    "cd71@listes.entrouvert.com",
    "cd85@listes.entrouvert.com",
    "cd91@listes.entrouvert.com",
    "chambery@listes.entrouvert.com",
    "chatou@listes.entrouvert.com",
    "clermont-metropole@listes.entrouvert.com",
    "cnil@listes.entrouvert.com",
    "crous-bfc@listes.entrouvert.com",
    "ehess@listes.entrouvert.com",
    "estensemble@listes.entrouvert.com",
    "grand-lyon-gi@listes.entrouvert.com",
    "grand-nancy@listes.entrouvert.com",
    "grenoble-peps@listes.entrouvert.com",
    "isere@listes.entrouvert.com",
    "landes@listes.entrouvert.com",
    "lanester@listes.entrouvert.com",
    "la-rochelle@listes.entrouvert.com",
    "leplessistrevise@listes.entrouvert.com",
    "le-havre@listes.entrouvert.com",
    "lille@listes.entrouvert.com",
    "malakoff@listes.entrouvert.com",
    "marcoussis@listes.entrouvert.com",
    "martigues@listes.entrouvert.com",
    "mel@listes.entrouvert.com",
    "mines-paris-psl@listes.entrouvert.com",
    "minint@listes.entrouvert.com",
    "minsol@listes.entrouvert.com",
    "montelimar-agglo@listes.entrouvert.com",
    "nantes-metropole@listes.entrouvert.com",
    "nice@listes.entrouvert.com",
    "nimes@listes.entrouvert.com",
    "pays-herbiers@listes.entrouvert.com",
    "sablesdolonne@listes.entrouvert.com",
    "saint-laurent-du-var@listes.entrouvert.com",
    "seine-eure@listes.entrouvert.com",
    "sictiam@listes.entrouvert.com",
    "tco@listes.entrouvert.com",
    "thouare@listes.entrouvert.com",
    "toulon@listes.entrouvert.com",
    "toulouse-metropole@listes.entrouvert.com",
    "toulouse-parsifal@listes.entrouvert.com",
    "unistra@listes.entrouvert.com",
    "univ-avignon@listes.entrouvert.com",
    "univ-nantes@listes.entrouvert.com",
    "univ-savoie-mont-blanc@listes.entrouvert.com",
    "vaucluse@listes.entrouvert.com",
    "villeneuve-d-ascq@listes.entrouvert.com",
    "ville-de-grenoble@listes.entrouvert.com",
    "ville-montpellier@listes.entrouvert.com",
    "3m@listes.entrouvert.com"
    ])
{
    fileinto "clients-reste";
}

# The command "keep" is executed automatically, if no other action is taken.
