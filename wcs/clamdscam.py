# wcs-manage runscript -d wcs.dev.publik.love clamdscam.py --formdata-url https://wcs.dev.publik.love/backoffice/management/xxx/yy/ --filename foo.txt --clamd-returncode 2

import argparse
import pprint
import sys

from django.utils.timezone import now

from wcs.formdef import FormDef
from wcs.qommon.misc import localstrftime


parser = argparse.ArgumentParser()
parser.add_argument(
    '--formdata-url',
    required=True,
    help='formdata url (frontoffice or backoffice)',
)
parser.add_argument('--only-show', action='store_true', help='only show clamd infos')
parser.add_argument('--filename', required=True, help='target filename')
parser.add_argument(
    '--waiting-scan', action='store_true', help='mark the file as beeing waiting for scan'
)
parser.add_argument(
    '--clamd-returncode',
    type=int,
    choices=[0, 1, 2, 100],
    help='0 (No virus found), 1 (Virus found), 2 (An error occurred), 100 (Forced ok)',
)
args = parser.parse_args(sys.argv[1:])

formdata_list = args.formdata_url.split('/')
if not formdata_list[-1]:
    formdata_list.pop()
formdef_slug, formdata_id = formdata_list[-2], formdata_list[-1]

try:
    formdef = FormDef.get_by_urlname(formdef_slug)
except KeyError:
    sys.stdout.write(f'formdef with slug {formdef_slug} not found\n')
    sys.exit(1)
try:
    formdata = formdef.data_class().get(formdata_id)
except KeyError:
    sys.stdout.write(f'formdata with id {formdata_id} not found\n')
    sys.exit(1)

for file_data in formdata.get_all_file_data(with_history=False):
    if file_data.orig_filename == args.filename:
        if args.only_show:
            pprint.pp(file_data.clamd, sys.stdout)
        else:
            file_data.clamd['returncode'] = args.clamd_returncode
            file_data.clamd['timestamp'] = localstrftime(now())
            file_data.clamd['clamdscan_output'] = 'clamdscam'
            file_data.clamd['waiting_scan'] = args.waiting_scan
            formdata._store_all_evolution = True
            formdata.store()
            sys.stdout.write('File successfully scamed\n')
        sys.exit(0)

sys.stdout.write('File not found\n')
sys.exit(1)
