#!/bin/bash

set -x

rsync -v -a --rsync-path="sudo rsync" authentic.node1.prod.saas.entrouvert.org:/var/lib/authentic2-multitenant/tenants/connexion.services.metzmetropole.fr/ /var/lib/authentic2-multitenant/tenants/connexion.services.metzmetropole.fr

rsync -v -a --rsync-path="sudo rsync" bijoe.node1.prod.saas.entrouvert.org:/var/lib/bijoe/tenants/statistiques.services.metzmetropole.fr/ /var/lib/bijoe/tenants/statistiques.services.metzmetropole.fr

rsync -v -a --rsync-path="sudo rsync" chrono.node1.prod.saas.entrouvert.org:/var/lib/chrono/tenants/agendas.services.metzmetropole.fr/ /var/lib/chrono/tenants/agendas.services.metzmetropole.fr

rsync -v -a --rsync-path="sudo rsync" combo.node1.prod.saas.entrouvert.org:/var/lib/combo/tenants/agents.services.metzmetropole.fr/ /var/lib/combo/tenants/agents.services.metzmetropole.fr

rsync -v -a --rsync-path="sudo rsync" combo.node1.prod.saas.entrouvert.org:/var/lib/combo/tenants/services.metzmetropole.fr/ /var/lib/combo/tenants/services.metzmetropole.fr

rsync -v -a --rsync-path="sudo rsync" fargo.node1.prod.saas.entrouvert.org:/var/lib/fargo/tenants/porte-doc.services.metzmetropole.fr/ /var/lib/fargo/tenants/porte-doc.services.metzmetropole.fr

rsync -v -a --rsync-path="sudo rsync" hobo.node1.prod.saas.entrouvert.org:/var/lib/hobo/tenants/hobo.services.metzmetropole.fr/ /var/lib/hobo/tenants/hobo.services.metzmetropole.fr

rsync -v -a --rsync-path="sudo rsync" passerelle.node1.prod.saas.entrouvert.org:/var/lib/passerelle/tenants/passerelle.services.metzmetropole.fr/ /var/lib/passerelle/tenants/passerelle.services.metzmetropole.fr

rsync -v -a --rsync-path="sudo rsync" wcs.node1.prod.saas.entrouvert.org:/var/lib/wcs/demarches.services.metzmetropole.fr/ /var/lib/wcs/demarches.services.metzmetropole.fr


# theme -> /usr/share/publik/themes/publik-base doit devenir /usr/local/share/publik-devinst/themes/publik-base
VAR_LIBS='/var/lib/authentic2-multitenant/tenants/connexion.services.metzmetropole.fr /var/lib/bijoe/tenants/statistiques.services.metzmetropole.fr  /var/lib/chrono/tenants/agendas.services.metzmetropole.fr agents.services.metzmetropole.fr /var/lib/combo/tenants/services.metzmetropole.fr /var/lib/combo/tenants/agents.services.metzmetropole.fr /var/lib/fargo/tenants/porte-doc.services.metzmetropole.fr /var/lib/hobo/tenants/hobo.services.metzmetropole.fr /var/lib/passerelle/tenants/passerelle.services.metzmetropole.fr  /var/lib/wcs/demarches.services.metzmetropole.fr'

for directory in $VAR_LIBS; do
    theme="$directory/theme"
    if [ -L $theme ]; then
        rm $theme
    fi
    ln -s /usr/local/share/publik-devinst/themes/publik-base $theme
done
    
# change wcs database setting
/home/cazino/envs/publik-env-py3/bin/wcs-manage runscript -d demarches.services.metzmetropole.fr change_db_settings.py
sudo supervisorctl restart wcs
/home/cazino/envs/publik-env-py3/bin/wcs-manage runscript -d demarches.services.metzmetropole.fr show_db_settings.py
