from quixote import get_publisher

pub = get_publisher()
postgres_conf = pub.cfg['postgresql']
del postgres_conf['password']
postgres_conf['host'] = ''
postgres_conf['user'] = ''
pub.write_cfg()
