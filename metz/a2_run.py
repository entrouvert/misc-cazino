"""
Usage :

sudo -u authentic-multitenant authentic2-multitenant-manage tenant_command runscript a2_run.py -d connexion-metz.test.entrouvert.org

sudo -u authentic-multitenant authentic2-multitenant-manage tenant_command runscript a2_run.py -d connexion.services.metzmetropole.fr
"""

from django.contrib.auth import get_user_model


User = get_user_model()


def build_email(first_name, last_name):
    return 'vpf-%s-%s@yopmail.com' % (first_name, last_name)


def main():
    for user in User.objects.filter(email__endswith='metzmetropole.fr'):
        old_email = user.email
        prefix, domain = old_email.split('@')
        new_email = '%s@%s' % (prefix, 'eurometropolemetz.eu')
        user.username = old_email
        user.email = new_email
        print('%s %s %s %s' % (user.first_name, user.last_name, user.username, user.email))
        user.save()


main()
