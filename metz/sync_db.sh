#!/bin/bash

set -xe

# DROP LOCAL schemas
psql -c "DROP SCHEMA IF EXISTS connexion_services_metzmetropole_fr CASCADE;" authentic2_multitenant
psql -c "DROP SCHEMA IF EXISTS statistiques_services_metzmetropole_fr CASCADE;" bijoe
psql -c "DROP SCHEMA IF EXISTS demarches_services_metzmetropole_fr CASCADE;" bijoe
psql -c "DROP SCHEMA IF EXISTS agendas_services_metzmetropole_fr CASCADE;" chrono
psql -c "DROP SCHEMA IF EXISTS agents_services_metzmetropole_fr CASCADE;" combo
psql -c "DROP SCHEMA IF EXISTS services_metzmetropole_fr CASCADE;" combo
psql -c "DROP SCHEMA IF EXISTS porte_doc_services_metzmetropole_fr CASCADE;" fargo
psql -c "DROP SCHEMA IF EXISTS hobo_services_metzmetropole_fr CASCADE;" hobo
psql -c "DROP SCHEMA IF EXISTS passerelle_services_metzmetropole_fr CASCADE;" passerelle
psql -c "DROP DATABASE IF EXISTS wcs_demarches_services_metzmetropole_fr;"




# pg_dump prod schemas
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --schema=connexion_services_metzmetropole_fr --format=custom --file=/tmp/connexion_services_metzmetropole_fr.dump authentic2_multitenant"
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --schema=statistiques_services_metzmetropole_fr --format=custom --file=/tmp/statistiques_services_metzmetropole_fr.dump bijoe"
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --schema=demarches_services_metzmetropole_fr --format=custom --file=/tmp/demarches_services_metzmetropole_fr.dump bijoe"
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --schema=agendas_services_metzmetropole_fr --format=custom --file=/tmp/agendas_services_metzmetropole_fr.dump chrono"
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --schema=agents_services_metzmetropole_fr --format=custom --file=/tmp/agents_services_metzmetropole_fr.dump combo"
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --schema=services_metzmetropole_fr --format=custom --file=/tmp/services_metzmetropole_fr.dump combo"
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --schema=porte_doc_services_metzmetropole_fr --format=custom --file=/tmp/porte_doc_services_metzmetropole_fr.dump fargo"
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --schema=hobo_services_metzmetropole_fr --format=custom --file=/tmp/hobo_services_metzmetropole_fr.dump hobo"
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --schema=passerelle_services_metzmetropole_fr --format=custom --file=/tmp/passerelle_services_metzmetropole_fr.dump passerelle"
ssh db1.prod.saas.entrouvert.org "sudo -u postgres pg_dump --format=custom --file=/tmp/wcs_demarches_services_metzmetropole_fr.dump wcs_demarches_services_metzmetropole_fr"


# scp distant shemas
scp db1.prod.saas.entrouvert.org:/tmp/*.dump /tmp/

# clean distant schemas
ssh db1.prod.saas.entrouvert.org "sudo rm /tmp/*.dump"


# restore schemas localy
pg_restore --dbname=authentic2_multitenant --no-owner /tmp/connexion_services_metzmetropole_fr.dump
pg_restore --dbname=bijoe --no-owner /tmp/statistiques_services_metzmetropole_fr.dump
pg_restore --dbname=bijoe --no-owner /tmp/demarches_services_metzmetropole_fr.dump
pg_restore --dbname=chrono --no-owner /tmp/agendas_services_metzmetropole_fr.dump
pg_restore --dbname=combo --no-owner /tmp/agents_services_metzmetropole_fr.dump
pg_restore --dbname=combo --no-owner /tmp/services_metzmetropole_fr.dump
pg_restore --dbname=fargo --no-owner /tmp/porte_doc_services_metzmetropole_fr.dump
pg_restore --dbname=hobo --no-owner /tmp/hobo_services_metzmetropole_fr.dump
pg_restore --dbname=passerelle --no-owner /tmp/passerelle_services_metzmetropole_fr.dump
psql -c "CREATE DATABASE wcs_demarches_services_metzmetropole_fr WITH OWNER = cazino;"
pg_restore --dbname=wcs_demarches_services_metzmetropole_fr --no-owner /tmp/wcs_demarches_services_metzmetropole_fr.dump
