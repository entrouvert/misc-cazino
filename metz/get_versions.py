import requests


URLS = {
    'authentic': 'https://connexion.services.metzmetropole.fr/',
    'bijoe': 'https://statistiques.services.metzmetropole.fr/',
    'chrono': 'https://agendas.services.metzmetropole.fr/',
    'combo': 'https://services.metzmetropole.fr/',
    'hobo': 'https://hobo.services.metzmetropole.fr/',
    'passerelle': 'https://passerelle.services.metzmetropole.fr/',
    'wcs': 'https://demarches.services.metzmetropole.fr/',
}

for service, url in URLS.items():
    url = url = '%s__version__' % url
    response = requests.get(url, timeout=5, verify=True)
    response.raise_for_status()
    json = response.json()

    if service == 'authentic':
        print('authentic_version: "v%s"' % json['authentic2'])
        print('django_mellon_version: "v%s"' % json['python-django-mellon'])
        print('django_tenant_schemas_version: "v%s"' % json['django-tenant-schemas'])
        print('gadjo_version: "v%s"' % json['gadjo'])

    if service == 'bijoe':
        print('bijoe_version: "v%s"' % json['bijoe'])
        print('wcs_olap_version: "v%s"' % json['wcs-olap'])

    if service == 'chrono':
        print('chrono_version: "v%s"' % json['chrono'])

    if service == 'passerelle':
        print('passerelle_version: "v%s"' % json['passerelle'])

    if service == 'combo':
        print('combo_version: "v%s"' % json['combo'])
        print('eopayment_version: "v%s"' % json['eopayment'])

    if service == 'hobo':
        print('hobo_version: "v%s"' % json['hobo'])

    if service == 'wcs':
        print('wcs_version: "v%s"' % json['wcs'])
        print('auquotidien_version: "v%s"' % json['wcs-au-quotidien'])
        print('publik_base_theme_version: "v%s"' % json['publik-base-theme'])
